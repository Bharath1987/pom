package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class EditLead extends Annotations {

	public EditLead EditButton() {
		//driver.findElementByXPath("//a[text()='Edit']").click();
		WebElement EditButton = locateElement("Xpath", "//a[text()='Edit']");
		click(EditButton);
		return this;
	}
	public EditLead CompanyNameUpdate(String UpdateCname) {
		//driver.findElementById("updateLeadForm_companyName").clear();
		//driver.findElementById("updateLeadForm_companyName").sendKeys(UpdateCname);
		WebElement CompanyNameUpdate = locateElement("Id", "updateLeadForm_companyName");
		clearAndType(CompanyNameUpdate, UpdateCname);
		return this;
	}
	public ViewLeads ClickUpdateButton() {
		//driver.findElementByClassName("smallSubmit").click();
		WebElement ClickUpdateButton = locateElement("Class", "smallSubmit");
		click(ClickUpdateButton);
		return new ViewLeads();
	}


}

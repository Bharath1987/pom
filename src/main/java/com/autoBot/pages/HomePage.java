package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;


public class HomePage extends Annotations{ 

	public HomePage verifyLoginName(String data) {
		WebElement ClickCreateLead = locateElement("tag", "h2");
		String LoginName = ClickCreateLead.getText();
		if(LoginName.contains(data)) {
			System.out.println("Login success");
		}else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
	
//	public LoginPage clickLogoutButton() {
//		driver.findElementByClassName("decorativeSubmit").click();
//		return new LoginPage();
//	}
	public MyHomePage ClickCRMSFA() {
		WebElement ClickCRMSFALink = locateElement("Xpath", "(//div[@id='label']//a[1])[1]");
		click(ClickCRMSFALink);
		return new MyHomePage();
	}
		
	

}








package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeads extends Annotations{
	public CreateLeads FirstName(String Fname) {
		//driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
		WebElement FirstName = locateElement("Id", "createLeadForm_firstName");
		clearAndType(FirstName, Fname);
		return this;
	}
	public CreateLeads LastName(String Lname) {
		//driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
		WebElement LastName = locateElement("Id", "createLeadForm_lastName");
		clearAndType(LastName, Lname);
		return this;
	}
	public CreateLeads CompanyName(String Cname) {
		//driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
		WebElement CompanyName = locateElement("Id", "createLeadForm_companyName");
		clearAndType(CompanyName, Cname);
		return this;
	}
	public ViewLeads ClickCreateLeadbutton() {
		//driver.findElementByClassName("smallSubmit").click();
		WebElement ClickCreateLeadbutton = locateElement("Class", "smallSubmit");
		click(ClickCreateLeadbutton);
		return new ViewLeads();
		
	}

}

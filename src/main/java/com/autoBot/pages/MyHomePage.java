package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {

	public CreateLeads ClickMyHomePageCreateLead() {
		//driver.findElementByLinkText("Create Lead").click();
		WebElement ClickCreateLead = locateElement("link", "Create Lead");
		click(ClickCreateLead);
		return new CreateLeads();
		
	}
}

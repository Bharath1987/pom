package com.autoBot.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class FindLead extends Annotations {
	public FindLead FindLeadsClick() {
		//driver.findElementByXPath("//a[text()='Find Leads']").click();
		WebElement FindLeadsClick = locateElement("Xpath", "//a[text()='Find Leads']");
		click(FindLeadsClick);
		return this;
	}

	public FindLead FirstName(String Fname) {
		//driver.findElementByXPath("((//label[text()='First name:'])[3]/following::input)[1]").sendKeys(Fname);
		WebElement FirstName = locateElement("Xpath", "((//label[text()='First name:'])[3]/following::input)[1]");
		clearAndType(FirstName, Fname);
		return this;
	}
	
	public FindLead FindLeadButton() {
		//driver.findElementByXPath("//button[text()='Find Leads']").click();
		WebElement FindLeadButton = locateElement("Xpath", "//button[text()='Find Leads']");
		click(FindLeadButton);
		return this;
	}

	public EditLead LeadListSelection() throws InterruptedException {
		Thread.sleep(5000);
		//driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		WebElement LeadListSelection = locateElement("Xpath", "(//a[@class='linktext'])[4]");
		click(LeadListSelection);
		//String Firstvalue = table.getText();
		//System.out.println(Firstvalue);
//		List<WebElement> rows = table.findElements(By.tagName("tr"));
//		int size = rows.size();
//		System.out.println(("sssssssss"+size));
//			for(int i=0; i<rows.size(); i++) {
//			WebElement firstrow = rows.get(i);
//			List<WebElement> column = firstrow.findElements(By.tagName("td"));
//			for(int j=0; j<column.size(); j++ ) {
//				String Columntext = column.get(0).getText();
//				if(Columntext.contains(null))
//				System.out.println(Columntext);
//				else
//					System.out.println("No text found");
		
			
			
		return new EditLead();
	}
}

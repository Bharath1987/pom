package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.FindLead;
import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC001_LoginAndLogout extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "Login Create Find Edit Verify leads";
		author = "Bharath";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd, String data, String Fname, String Lname, String Cname, String UpdateCname) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.verifyLoginName(data)
		.ClickCRMSFA()
		.ClickMyHomePageCreateLead()
		.FirstName(Fname)
		.LastName(Lname)
		.CompanyName(Cname)
		.ClickCreateLeadbutton()
		.VerifyViewLeads(Fname)
		.FindLeadsClick()
		.FirstName(Fname)
		.FindLeadButton()
		.LeadListSelection()
		.EditButton()
		.CompanyNameUpdate(UpdateCname);
		
		
		
		
		//.clickLogout();
		
	}
	
}






